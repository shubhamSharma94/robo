function Order (tableNum, orderNum, foodItems, orderStatus) {
    this.tableNum = tableNum;
    this.orderNum = orderNum;
    this.foodItems = foodItems;
    this.orderStatus = orderStatus;
}

module.exports.Order = Order;
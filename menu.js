const MENU_ITEMS = [
    {
        name: 'Bread',
        itemId: 1,
        price: 50,
    },
    {
        name: 'drinks',
        itemId: 2,
        price: 500,
    },
    {
        name: 'Rice',
        itemId: 3,
        price: 150,
    }
]

const showMenu = () => {
    return MENU_ITEMS;
}

module.exports.showMenu = showMenu;
module.exports.MENU_ITEMS = MENU_ITEMS;